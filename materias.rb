module Disciplinas

    class Materias
        attr_acessor :nome , :ementa
        def iniciar(nome:, ementa:, professores:)
            @nome = nome
            @ementa = ementa
            @professores = []
        end
        
        def addProf(professor)
            @professores.push(professor)
        end
    end

    class Turma
        attr_acessor :nome , :horario
        def iniciar(nome:, horario:, dsemana:, inscritos:, inscricao_aberta:)
            @nome = nome
            @horario = horario
            @dsemana = []
            @inscritos = []
            @inscricao_aberta = false
        end
        
        def abrir_inscricao()
            @inscricao_aberta = true
        end

        def fechar_inscricao()
            @inscricao_aberta = false
        end

        def addAluno(aluno)
            @inscritos.push(aluno)
        end
    end

