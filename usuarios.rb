module Usuarios #Um modulo para os Usuarios

  class Usuario
    attr_acessor :nome , :matricula
    def iniciar(nome:, matricula:, email:, senha:, cpf:, nascimento:, endereco:)
      @nome = nome
      @matricula = matricula
      @email = email
      @senha = senha
      @cpf = cpf
      @nascimento = nascimento.text_field(type: 'date')
      @endereco = endereco
      @online = false
    end
    def idade()
      now = Time.now.utc.to_date
      now.year - @nascimento.year - (@nascimento.to_date.change(:year => now.year) > now ? 1 : 0)
      return @nascimento
    end
    def entrar(email, senha)
      if(email.downcase == @email.downcase && senha == @senha)
        @online = true
      else
        puts("Cadastro não encontrado")
      end
    end
    def sair()
      @online = false
    end
  end

  class Aluno > Usuario
    def iniciar(nome:, matricula:, email:, senha:, cpf:, nascimento:, endereco:, periodo:, curso:)
      super(nome: nome, foto: foto, matricula: matricula, cpf: cpf, email: email, senha: senha, nascimento: nascimento, endereco: endereco)
      @turma = []
      @periodo = periodo
      @curso = curso
    end
      def inscrever(turma)
        @turma.push(turma)
      end
  end

  class Professor > Usuario
    def iniciar(nome:, matricula:, email:, senha:, cpf:, nascimento:, endereco:, salario:, materias:)
      super(nome: nome, foto: foto, matricula: matricula, cpf: cpf, email: email, senha: senha, nascimento: nascimento, endereco: endereco)
      @salario = salario
      @materias = []
    end
    def addMateria(materia)
      @materias.push(materia)
    end
  end